# newsfeed-recommender

A newsfeed recommender complimenting the newsboat reader

# Steps to use this

1. [Learn to use RSS feed and newsboat properly](https://yewtu.be/watch?v=dUFCRqs822w). Learn about filters, learn about flags from the [Documentation.](https://newsboat.org/releases/2.21/docs/newsboat.html)

2. Import your RSS feeds by either editing ~/.newsboat/urls or by importing OPML files.

3. After importing, fetch new feeds. Go through the articles, and set the flag s on articles to denote a 'like'. I have been using newsboat since a year now and has a database of 90,000 articles with 15,000 marked as starred by setting flags as s. The more the data, the better.

4. Run Building Models.ipynb so that you make a AI model to learn your preferences. Since newsboat uses sqlite, change the location of the databases accordingly in the iPython notebook so that correct datasets gets loaded. This takes some time, if your dataset is large.

5. Run Prediction.ipynb to make recommendations. Your recommendations gets exported to a latest.atom file

6. Update your latest.atom in a place where it is accessible from the internet. [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/) is a good place to host. 

7. Now you should have the link to the latest.atom file. You can import that in newsboat itself or your favorite web frontend RSS readers like FreshRSS, miniflux etc.

Please feel free to open an issue in case of any questions.
